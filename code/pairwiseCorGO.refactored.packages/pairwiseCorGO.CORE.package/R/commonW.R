commonW <-
	function(w1,w2,allpairs=FALSE) {
		commonWPairs<- if(!allpairs) intersect(paste(w1[,1],w1[,2],sep="_"),paste(w2[,1],w2[,2],sep="_")) else commonWPairs<-paste(w1[,1],w1[,2],sep="_")
		
		return(strsplit(commonWPairs,"_",fixed=TRUE))
	}