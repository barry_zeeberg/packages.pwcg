HTGM <-
	function(TVT,b,df.name,pthresh,DBG=FALSE,startMin=0.50) {
		# TVT is CIM of 0s and 1s indicating membership of genes in GO categories
		h<-hash()
		bins<-hash()
		m<-matrix(nrow=2,ncol=2) # HTGM 2 x 2 contingency table for Fisher's Exact
		rownames(m)<-c("in cat","not in cat")
		colnames(m)<-c("in changed","not in changed")
		
		write(paste("DATABASE ",df.name,sep=""),"")
		tgenes<-dim(TVT)[2]
		#starters<-as.numeric(keys(b$cwn))>=startMin||as.numeric(keys(b$cwn))<=-startMin
		starters<-as.numeric(keys(b$cwn))>=startMin
		for(start in keys(b$cwn)[starters]) {
			write(paste("START ",start,sep=""),"")
			f<-hash()
			#gen<-hash()
			
			summary<-matrix(nrow=dim(TVT)[1],ncol=8) # extended version of HTGM .change.xls file
			colnames(summary)<-c("tgenes","in cat in changed","enrichment","in cat not in changed","not in cat in changed","not in cat not in changed","p-value","FDR")
			rownames(summary)<-rownames(TVT)
			
			genes<-unique(as.vector(b$cwn[[start]]$commonWPairNames)) # genes in correlation bin AND mapping to common GO category
			ngenes<-length(genes)
			
			summary[,"tgenes"]<-rowSums(TVT)
			summary[,"in cat in changed"]<-rowSums(TVT[,genes])
			summary[,"enrichment"]<-(summary[,"in cat in changed"]/ngenes)/(summary[,"tgenes"]/tgenes)
			summary[,"in cat not in changed"]<-summary[,"tgenes"]-summary[,"in cat in changed"]
			summary[,"not in cat in changed"]<-ngenes-summary[,"in cat in changed"]
			summary[,"not in cat not in changed"]<-tgenes-(summary[,"in cat in changed"]+summary[,"in cat not in changed"]+summary[,"not in cat in changed"])
			
			w<-which(summary[,"in cat in changed"]>1)
			for(r in w) {
				m["in cat","in changed"]<-summary[r,"in cat in changed"]
				m["in cat","not in changed"]<-summary[r,"in cat not in changed"]
				m["not in cat","in changed"]<-summary[r,"not in cat in changed"]
				m["not in cat","not in changed"]<-summary[r,"not in cat not in changed"]
				fisher.both<-fisher.test(m)
				fisher.less<-fisher.test(m,alternative="less")
				fisher.greater<-fisher.test(m,alternative="greater")
				
				#print("DEBUG HTGM:")
				#print(c(r,rownames(summary)[r],fisher.greater$p.value))
				
				
				if(as.numeric(fisher.greater$p.value)<=pthresh) {
					gen<-hash()
					for(g in genes) {
						if(TVT[rownames(summary)[r],g]==1)
							gen[g]<-g # genes in (correlation bin AND mapping to common GO category) that map to each significant category
						}
					f[rownames(summary)[r]]<-hash(c("fisher","genes"),c(round(-log10(as.numeric(fisher.greater$p.value)),3),gen))
					}
				}
				
			if(DBG && start>0.6) {
				w<-which(summary[,"in cat in changed"]>0)
				#rn<-sample(rownames(summary[w,]),1)
				rn<-rownames(summary)[w[1]]
				write(paste("2 x 2 contingency table for ",rn),"")
				write(paste(summary[rn,"in cat in changed"],summary[rn,"not in cat in changed"],summary[rn,"in cat in changed"]+summary[rn,"not in cat in changed"],sep="\t"),"")
				write(paste(summary[rn,"in cat not in changed"],summary[rn,"not in cat not in changed"],summary[rn,"in cat not in changed"]+summary[rn,"not in cat not in changed"],sep="\t"),"")
				write(paste(summary[rn,"in cat in changed"]+summary[rn,"in cat not in changed"],summary[rn,"not in cat in changed"]+summary[rn,"not in cat not in changed"],"\t"),"")
				m["in cat","in changed"]<-summary[rn,"in cat in changed"]
				m["in cat","not in changed"]<-summary[rn,"in cat not in changed"]
				m["not in cat","in changed"]<-summary[rn,"not in cat in changed"]
				m["not in cat","not in changed"]<-summary[rn,"not in cat not in changed"]
				fisher<-fisher.test(m)
				} # if(DBG && start>0.6)
				
		bins[start]<-f	
		}
	h$bins<-bins
	# bins$start is hash whose keys are significant GO categories
	# each category is a hash whose keys are $fisher and $genes
	
	return(h)
	}
