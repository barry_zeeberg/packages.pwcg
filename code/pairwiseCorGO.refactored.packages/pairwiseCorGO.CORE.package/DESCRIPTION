Package: pairwiseCorGO.CORE.package
Type: Package
Title: Estimate the probability that pairs of expression-correlated genes share a common biological function.
Version: 1.0
Date: 2012-12-25
Author: Barry Zeeberg
Maintainer: Barry Zeeberg <barry@discover.nci.nih.gov>
Depends: R (>= 2.12.1)
Imports: pairwiseCorGO.CIM.package, pairwiseCorGO.SVG.package, pairwiseCorGO.DATA.package, pairwiseCorGO.TVT.JAC.package,zutilities.package, hash
Description: There is growing evidence that genes whose expression profiles are correlated often are involved in related biological functions. This package estimates the probability P that pairs of expression-correlated genes share a common biological function. First, the Gene Ontology (GO) is used to construct a matrix (GOM) of biological process categories versus genes, in which the entries were either 1 or 0 (gene present or not in category). Overly generic categories (e.g., categories containing >= N genes, where N was 100, 300, or 600) were excluded from GOM to reduce trivial associations from contaminating the results. Then the following procedure was performed separately for each of 3 differnt cell lines or tissue samples. The published gene expression profiles were used to compute a gene versus gene correlation matrix (CORM). For each gene pair, we used GOM to determine if there is at least one non-generic GO category to which both genes mapped and CORM to provide that pair's correlation. The tally of pos and neg GOM results were compiled into bins corresponding to correlation values -1.00 to -0.90, -0.90 to -0.80, up to 0.90 to 1.00; e.g., if a gene pair shared a common mapping to a GO category and had correlation 0.74, then that pos GO result was recorded by incrementing the tally of pos in the 0.70 to 0.80 bin. Finally, the fraction F=pos/(pos+neg) in each correlation bin was plotted. The background level B was taken as the average of F in bins -0.10 to 0 and 0 to 0.10. The result was visualized using Scalable Vector Graphics (SVG) technology with embedded hyperlinks to allow interactive examination of genes in various bins.
License: GPL(>=2)
LazyData: yes
LazyLoad: yes
Suggests: testthat
