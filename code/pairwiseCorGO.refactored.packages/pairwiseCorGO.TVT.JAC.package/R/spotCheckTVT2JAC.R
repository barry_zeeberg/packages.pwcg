spotCheckTVT2JAC<-
	function(TVT.HTGM.file,TVT.RData,JAC.dir,cat.size=20,niter=5,NOCOMAP=FALSE,FAKEGENE="GAS6") {
		# given a category size in HTGM TVT file
		# randomly select a category equal to that size
		# randomly select 2 genes in that category
		# show that the 2 genes co-map in TVT.RData file
		# show that the 2 genes co-map in JAC.RData file
		
		TVT.HTGM<-read.table(TVT.HTGM.file,sep="\t",stringsAsFactors=FALSE,quote="")
		cs<-sort(table(TVT.HTGM[,1]),decreasing=TRUE)
		
		TVT<-get(load(TVT.RData))
		
		ptvt<-parseTVTname(TVT.RData)
		
		JAC.RData<-constructJACname(JAC.dir,ptvt)
		JAC<-get(load(JAC.RData))
		
		for(iter in 1:niter) {
			print(paste("iter",iter,sep=" "))
			# randomly select a category equal to cat.size
			w<-which(cs==cat.size)
			if(length(w)==0)
				stop(c("spotCheckTVT2JAC:","no categories of size",cat.size))
			cat.rand<-sample(w,1)
			print(paste("spotCheckTVT2JAC:","random category",cs[cat.rand],cat.size,sep=" "))
			w<-which(TVT.HTGM[,1]==names(cat.rand))
		
			print(paste("spotCheckTVT2JAC","TVT.HTGM[w,]:",sep=" "))
			print(TVT.HTGM[w,])
		
			# randomly select 2 genes in that category
			genes.rand<-sample(TVT.HTGM[w,2],2)
			print(paste("spotCheckTVT2JAC","random genes:",sep=" "))
			print(genes.rand)
			
			if(iter==niter) # on last iteration, make sure test fails when non-comapping gene is substituted for one of the random genes
				if(NOCOMAP) {
					print(paste("spotCheckTVT2JAC","NOCOMAP option",FAKEGENE,sep=" "))
					genes.rand[2]<-FAKEGENE
					print(genes.rand)
					}
						
			# do these 2 genes show up as mapping to the category in TVT.RData?
			print(paste("spotCheckTVT2JAC","TVT[names(cat.rand),genes.rand]:",sep=" "))
			print(TVT[names(cat.rand),genes.rand])
			flag1<-TVT[names(cat.rand),genes.rand[1]]!=1
			if(flag1)
				print(paste("spotCheckTVT2JAC","TVT[names(cat.rand),genes.rand[1]]!=1",sep=" "))
			flag2<-TVT[names(cat.rand),genes.rand[2]]!=1
			if(flag2)
				print(paste("spotCheckTVT2JAC","TVT[names(cat.rand),genes.rand[2]]!=1",sep=" "))
		
			# do these 2 genes show up as co-mapping in JAC.RData?
			print(paste("spotCheckTVT2JAC","JAC[genes.rand[1],genes.rand[2]]:",sep=" "))
			print(paste(genes.rand[1],genes.rand[2],JAC[genes.rand[1],genes.rand[2]],sep=" "))
			flag3<-JAC[genes.rand[1],genes.rand[2]]==1
			if(flag3)
				print(paste("spotCheckTVT2JAC","JAC[genes.rand[1],genes.rand[2]]==1",sep=" "))
			if(flag1 | flag2 | flag3)
				print(paste("spotCheckTVT2JAC","EXPECTED FAILURE IF FINAL ITERATION",iter,"OF",niter,"ITERATIONS",sep=" "))
			}
		}