tvt.HTGM.ngenes<-
	function(tvt.HTGM.file,DBG=FALSE) {
		# return the total number of genes mapped in e.g. GO BP ontology 
		tvt.HTGM.table<-read.table(file=tvt.HTGM.file,header=FALSE,sep="\t",stringsAsFactors=FALSE,quote="")
		colnames(tvt.HTGM.table)<-c("category","gene","changed","gene2")
		genes<-unique(tvt.HTGM.table[,"gene"])
		ngenes<-length(genes)
		if(DBG) {
			print(c("tvt.HTGM.ngenes","dim(tvt.HTGM.table)",dim(tvt.HTGM.table)))
			print(tvt.HTGM.table[1:3,])
			print(c("tvt.HTGM.ngenes","number of unique genes:",ngenes))
			}
		
		return(ngenes)
		}