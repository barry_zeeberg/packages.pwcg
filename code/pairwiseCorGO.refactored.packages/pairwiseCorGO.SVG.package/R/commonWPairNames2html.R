commonWPairNames2html <-
	function(pwcg,database,start,file) {
		if(!is.null((pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]]))) {
			write("<html>",file)
			write(paste("<b>",database,start,"</b>",sep=" "),file,append=TRUE)
			write("<table border cellpadding=3>",file,append=TRUE)
		
			# interchange col1 and col2, then concatenate to original
			# the idea is that each column will have a complete set of the individual paired genes
			rev<-cbind(pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]][,2],pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]][,1])
			dbl<-rbind(pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]],rev)
			rownames(dbl)<-as.character(1:(dim(dbl)[1]))
			srt<-dbl[names(sort(dbl[,1])),]
			
			print(c("DIMS",dim(rev),dim(dbl),dim(srt)))
			
			for(r in 1:(dim(srt)[1]))
				write(paste("<tr><td>",srt[r,1],"<td>",srt[r,2] ,sep=""),file,append=TRUE)
		
			#for(r in 1:dim(pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]])[1])
				#write(paste("<tr><td>",pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]][r,1],"<td>",pwcg[[database]][["cwn"]][[as.character(start)]][["commonWPairNames"]][r,2] ,sep=""),file,append=TRUE)
			write("</table>",file,append=TRUE)
			write("</html>",file,append=TRUE)
			}
		}