\name{integrativeCIM}
\alias{integrativeCIM}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
integrativeCIM
}
\description{
Generate an integrative CIM image file.
}
\usage{
integrativeCIM(dir.integrative, pwcg, pthresh, nthresh, DBG = FALSE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{dir.integrative}{
Full path name of the directory into which to write the integrative CIM image file.
}
  \item{pwcg}{
Return value of pairwiseCorGO.
}
  \item{pthresh}{
Acceptance threshold Fisher's Exact p-values in integrative CIM.
}
  \item{nthresh}{
Acceptance threshold for number of categories meeting pthresh.
}
  \item{DBG}{
Boolean if TRUE output some debugging information.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Returns no value, but has side effect of generating an integrative CIM image file whose values are -log10(Fisher's Exact p-values).
}
\references{
The Gene Ontology (GO) project in 2006. Nucleic Acids Res 2006, 34(Database issue):D322-326.

Ashburner M, Ball CA, Blake JA, Botstein D, Butler H, Cherry JM, Davis AP, Dolinski K, Dwight SS, Eppig JT et al: Gene ontology: tool for the unification of biology. The Gene Ontology Consortium. Nat Genet 2000, 25(1):25-29.

Zeeberg BR, Qin H, Narasimhan S, Sunshine M, Cao H, Kane DW, Reimers M, Stephens RM, Bryant D, Burt SK et al: High-Throughput GoMiner, an 'industrial-strength' integrative gene ontology tool for interpretation of multiple-microarray experiments, with application to studies of Common Variable Immune Deficiency (CVID). BMC Bioinformatics 2005, 6:168

HTGM parameters are described at \url{http://discover.nci.nih.gov/gominer/hi-thruput-defs.jsp}
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
