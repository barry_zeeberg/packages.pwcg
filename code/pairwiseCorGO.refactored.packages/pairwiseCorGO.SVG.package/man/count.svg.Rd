\name{count.svg}
\alias{count.svg}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
count.svg
}
\description{
add table of GO category sizes to svg plot
}
\usage{
count.svg(pwcg, minsize, maxsize, file, ontology, cols, databases.name)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{pwcg}{
Return value of pairwiseCorGO.
}
  \item{minsize}{
min size of GO category to be included in output table
}
  \item{maxsize}{
max size of GO category to be included in output table
}
  \item{file}{
name of svg output file
}
  \item{ontology}{
"BP" (biological process), "CC" (cellular component), or "MF" (molecular function), indicating one of the three main branches of the Gene Ontology (GO).
}
  \item{cols}{
Vector containing an encoded color scale.
}
  \item{databases.name}{
character string name of database e.g. "sanger". most useful when "databases" parameter is hash of tissue subtypes of a single set of cell lines.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
returns no value, but has side effect of adding table to svg plot
}
\references{
The Gene Ontology (GO) project in 2006. Nucleic Acids Res 2006, 34(Database issue):D322-326.

Ashburner M, Ball CA, Blake JA, Botstein D, Butler H, Cherry JM, Davis AP, Dolinski K, Dwight SS, Eppig JT et al: Gene ontology: tool for the unification of biology. The Gene Ontology Consortium. Nat Genet 2000, 25(1):25-29.

Zeeberg BR, Qin H, Narasimhan S, Sunshine M, Cao H, Kane DW, Reimers M, Stephens RM, Bryant D, Burt SK et al: High-Throughput GoMiner, an 'industrial-strength' integrative gene ontology tool for interpretation of multiple-microarray experiments, with application to studies of Common Variable Immune Deficiency (CVID). BMC Bioinformatics 2005, 6:168

HTGM parameters are described at \url{http://discover.nci.nih.gov/gominer/hi-thruput-defs.jsp}

SVG technology is described at \url{http://www.adobe.com/svg/}
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
