expressionCIM<-
	function(geneLists,databases,dir.expression="~/",dir.correlation="~") {
		geneList<-list.files(geneLists,full.names=TRUE)		
		for(gl in geneList) {
			fname<-basename(gl)
			db<-strsplit(fname,"__",fixed=TRUE)[[1]][1]
			
			# for CCLE subtypes need to extract tissue type from eg "BREAST_0.5.txt"
			db<-strsplit(db,"_0",fixed=TRUE)[[1]][1]
			
			g<-scan(gl,"character")
			x<-databases[[db]][intersect(g,rownames(databases[[db]])),]
			
			# 01.01.16 change rescale=TRUE to rescale=FALSE			
			zhms<-zheatmapShell(x=x,ifile2=paste(dir.expression,fname,sep="/"),symm=FALSE,rescale=FALSE)
			
			# 01.02.16 generate correlation CIM
			zhms<-zheatmapShell(x=correlation(t(x)),ifile2=paste(dir.correlation,fname,sep="/"),symm=TRUE,rescale=FALSE)
			}
		}
		

