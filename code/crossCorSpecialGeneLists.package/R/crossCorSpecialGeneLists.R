crossCorSpecialGeneLists<-
	function(dir.special=system.file("extdata",package="specialGeneLists.package"),list.ordinary=NULL,dir.ordinary=NULL,dir.out="~",sep="^",N=11974,ZHMS=FALSE,which.norm="both") {
		# N is total number of genes annotated in GO biological process as of approximately 07.15.13
		# this value is 9188 for the older version of the GO database (12.01.12), that is still currently used by HTGM
		# this value is 11974 for the newer version of the GO database (12.04.15)
		# for details, see documentation about "ngenes.GO" inside R source file inventory.rerun.zheat()
		# on 01.06.16 I hardwired in 11974 for the newer version of the GO database
		
		print(c("DBG crossCorSpecialGeneLists WHICH.NORM:",which.norm))
		file.length.print(dir.special)
		
		if(is.null(list.ordinary) && is.null(dir.ordinary))
			stop("at least one of list.ordinary or dir.ordinary must be specified")
			
		if(!is.null(dir.ordinary))
			list.ordinary<-union(list.ordinary,list.files(dir.ordinary,full.names=TRUE))
			
		contingency.table<-matrix(nrow=2,ncol=2)
		rownames(contingency.table)<-c("in.special","not.in.special")
		colnames(contingency.table)<-c("in.ordinary","not.in.ordinary")
		
		print(c("LIST",list.ordinary))
		print(c("DIR",dir.ordinary))
		
		dateStr <- gsub(":", ".", gsub(" ", ".", date()))
		
		ccsgl<-hash()

		special<-getFiles(dir.special)
		m.lint<-matrix(nrow=length(keys(special))+1,ncol=length(list.ordinary)+1)
		m.norm<-matrix(nrow=length(keys(special))+1,ncol=length(list.ordinary)+1)
		m.pval<-matrix(nrow=length(keys(special))+1,ncol=length(list.ordinary)+1)
		rownames(m.lint)<-c("ngene",keys(special))
		colnames(m.lint)<-c("ngene",basename(list.ordinary))
		rownames(m.norm)<-c("ngene",keys(special))
		colnames(m.norm)<-c("ngene",basename(list.ordinary))
		rownames(m.pval)<-c("ngene",keys(special))
		colnames(m.pval)<-c("ngene",basename(list.ordinary))
		
		h<-hash()
		
		for(sp in keys(special)) {
			bsp<-basename(sp)
			lsp<-length(special[[sp]])
			m.lint[bsp,"ngene"]<-lsp
			m.norm[bsp,"ngene"]<-lsp
			m.pval[bsp,"ngene"]<-lsp
			for(ord in list.ordinary) {
				bord<-basename(ord)
				#int1<-inter(special[[sp]],scan(ord,"character",quiet=TRUE))
				int1<-inter(special[[sp]],scan(ord,"character",quiet=TRUE),which.norm)
				m.lint["ngene",bord]<-round(length(scan(ord,"character",quiet=TRUE)),0)
				m.norm["ngene",bord]<-length(scan(ord,"character",quiet=TRUE))
				m.pval["ngene",bord]<-round(length(scan(ord,"character",quiet=TRUE)),0)
				h[paste(bsp,bord,sep=sep)]<-int1$list

				m.lint[bsp,bord]<-int1$lint
				m.norm[bsp,bord]<-round(int1$norm,3)
				
				contingency.table["in.special","in.ordinary"]<-int1$lint
				contingency.table["in.special","not.in.ordinary"]<- lsp - int1$lint
				contingency.table["not.in.special","in.ordinary"]<- m.lint["ngene",bord] - int1$lint
				contingency.table["not.in.special","not.in.ordinary"]<- N - lsp - m.lint["ngene",bord] + int1$lint # see runFisher() in HTGM2D package
				
				fish<-fisher.test(contingency.table,alternative="greater")
				
				#print(contingency.table)
				#print(fish)
				
				m.pval[bsp,bord]<-round(-log10(fish$p.value),3)
				}
			}
		
		x<-m.pval[-1,-1]
		##print(x[1:3,1:3])
		w<-which(x==Inf,arr.ind=TRUE)
		print(w)
		##stop()
										
		ccsgl$m.lint<-m.lint
		ccsgl$m.norm<-m.norm
		ccsgl$m.pval<-m.pval
		ccsgl$int<-h

		fname<-paste("crossCorSpecialGeneLists",dateStr,"xls",sep=".")
		file<-paste(dir.out,fname,sep="/")
		print(c("SAVING FILE AS",file))
		write("CROSS CORRELATION COUNTS\n",file=file)
		write.table(x=m.lint,file=file,append=TRUE,quote=FALSE,sep="\t",col.names = NA, row.names = TRUE)
		write("\n\nCROSS CORRELATION NORMALIZED COUNTS\n",file=file,append=TRUE)
		write.table(x=m.norm,file=file,append=TRUE,quote=FALSE,sep="\t",col.names = NA, row.names = TRUE)
		write("\n\nCROSS CORRELATION -LOG10(P-VALUE)\n",file=file,append=TRUE)
		write.table(x=m.pval,file=file,append=TRUE,quote=FALSE,sep="\t",col.names = NA, row.names = TRUE)
		
		# added on 01.08.16
		save(ccsgl,file=paste(dir.out,"ccsgl.RData",sep="/"))
		
		if(ZHMS) {
			zhms<-zheatmapShell(x=m.lint[-1,-1],symm=FALSE,identifier="cross_correlation_counts",ifile2=paste(dir.out,"crossCorSpecialGenes",sep="/"),label="cross correlation counts")
			zhms<-zheatmapShell(x=m.lint[-1,-1],symm=FALSE,identifier="semiclustered.cross_correlation_counts",ifile2=paste(dir.out,"crossCorSpecialGenes",sep="/"),label="cross correlation counts",Rowv=NA)
			zhms<-zheatmapShell(x=m.norm[-1,-1],symm=FALSE,identifier="cross_correlation_normalized_counts",ifile2=paste(dir.out,"crossCorSpecialGenes",sep="/"),label="cross correlation normalized counts")
			zhms<-zheatmapShell(x=m.norm[-1,-1],symm=FALSE,identifier="semiclustered.cross_correlation_normalized_counts",ifile2=paste(dir.out,"crossCorSpecialGenes",sep="/"),label="cross correlation normalized counts",Rowv=NA)
			zhms<-zheatmapShell(x=m.pval[-1,-1],symm=FALSE,identifier="cross_correlation_pval",ifile2=paste(dir.out,"crossCorSpecialGenes",sep="/"),label="-log10(cross correlation pval)")
			zhms<-zheatmapShell(x=m.pval[-1,-1],symm=FALSE,identifier="semiclustered.cross_correlation_pval",ifile2=paste(dir.out,"crossCorSpecialGenes",sep="/"),label="-log10(cross correlation pval)",Rowv=NA)
			}


		print("inside CCSGL: file.length.print dir.special:")
		file.length.print(dir.special)


		return(ccsgl)
	}

getFiles<-
	function(dir) {
		h<-hash()
		files<-list.files(dir,full.names=TRUE)
		for(file in files)
			h[basename(file)]<-scan(file,"character",quiet=TRUE)
			
		return(h)
		}

inter<-
	function(v1,v2,which.norm="both") {
		h<-hash()
		
		int.list<-intersect(v1,v2)
		lint<-length(int.list)
		#int.norm<-lint/min(length(v1),length(v2)) # picks up subset/superset relationship
		
		denom<-if(which.norm=="first") length(v1) else if(which.norm=="second") length(v2) else min(length(v1),length(v2)) # picks up subset/superset relationship
		int.norm<-lint/denom
		
		h$list<-int.list
		h$lint<-lint
		h$norm<-int.norm
		
		return(h)
		}