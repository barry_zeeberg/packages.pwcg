\name{parse.inv.name}
\alias{parse.inv.name}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
parse.inv.name
}
\description{
return a character string containing min category size, max category size, expression threshold, and intersection mode (see inventory.rerun.zheat())
}
\usage{
parse.inv.name(bn, format = "new", rows = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{bn}{
character string containing the basename of a subdirectory of the form "inventory.rerun.zheat_BP_5_0025_CCLE_4_5_25_4way_Wed.Aug.26.01.40.33.2015", where "5" is the min category size, "0025" is the max category size, "4" is the expression threshold, and "4way" refers to 4way intersection (see inventory.rerun.zheat())
}
 \item{format}{
character string "old" means parse.inv.name() parses according to old file name format (prior to late December 2015) like "inventory.rerun.zheat_BP_5_0025_CCLE_4_5_25_4way_Wed.Aug.26.01.40.33.2015", "new" means format like "inventory.rerun.zheat_new.HTGM.database.build_BP_5_0100_CCLE_4_5_25_5way_Thu.Dec.31.08.18.23.2015"
}
 \item{rows}{
if FALSE return a single pasted string, otherwise break up into several rows to better fit in subsequent table format
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
returns a character string containing min category size, max category size, expression threshold, and intersection mode (see inventory.rerun.zheat())
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{inventory.rerun.zheat}}
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
