\name{kohn32.plos.migr.fig.3}
\alias{kohn32.plos.migr.fig.3}
\docType{data}
\title{
kohn32.plos.migr.fig.3
}
\description{
vector of gene names
}
\format{
  The format is:
 chr [1:N]
}
\details{
list of 24 migration genes given in Figure 3 of Kohn reference. These are the highest cross-correlating genes of a set of 32 migration genes, as described in the legend to Figure 3.
}
\source{
http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0035716
}
\references{
Gene Expression Profiles of the NCI-60 Human Tumor Cell Lines Define Molecular Interaction Networks Governing Cell Migration Processes
Kurt W. Kohn , Barry R. Zeeberg, William C. Reinhold, Margot Sunshine, Augustin Luna, Yves Pommier

Published: May 3, 2012DOI: 10.1371/journal.pone.0035716
}
\keyword{datasets}
